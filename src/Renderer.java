import javax.media.opengl.GL2;
import javax.media.opengl.glu.GLU;
import java.awt.*;
import java.nio.FloatBuffer;

import static java.lang.Math.*;

public class Renderer {
    private static GL2 gl2;
    private static int width, height, circleX, circleY, rectangleX, rectangleY;
    private static float centerX, centerY, radius;
    private static Color selectedColor;
    private static final float[] brightnessWidgetPosition = {0.85f, 0.95f, 0.1f, 0.9f};

    /**
     * OpenGL сам не умеет работать с HSV. Поможем ему.
     * @param hue - тон от 0 до 360
     * @param saturation - насыщенность от 0 до 100
     * @param value - яркость от 0 до 100
     */
    private static void setHSVColor(float hue, float saturation, float value){
        Color c = Color.getHSBColor(hue/360f, saturation/100f, value/100f);
        gl2.glColor3ub((byte)c.getRed(), (byte)c.getGreen(), (byte)c.getBlue());
    }

    /**
     * Установка начальных значений
     * @param canvas - компонент OpenGL
     * @param width - его ширина
     * @param height и длина
     */
    protected static void setup(GL2 canvas, int width, int height){
        Renderer.gl2 = canvas;
        gl2.glMatrixMode(GL2.GL_PROJECTION);
        gl2.glLoadIdentity();
        Renderer.width = width;
        Renderer.height = height;
        centerX = width*0.4f;
        centerY = height*0.5f;
        radius = height*0.3f;
        circleX =  (int) centerX;
        circleY = (int) centerY;
        rectangleY = (int) (height * brightnessWidgetPosition[3]) - 1;
        rectangleX = (int) (width * (brightnessWidgetPosition[0]+brightnessWidgetPosition[1])/2);
        GLU glu = new GLU();
        glu.gluOrtho2D( 0.0f, width, 0.0f, height );
        gl2.glMatrixMode(GL2.GL_MODELVIEW);
        gl2.glLoadIdentity();
        gl2.glViewport( 0, 0, width, height );
    }

    /**
     * Остновной метод
     * @param x, y - координаты клика мыши
     */
    protected static void render(int x, int y){
        makeCircle();
        performClick(x, height-y);
        showResult();
    }

    /**
     * отрисовка круга с цветами максимальной яркости
     */
    protected static void makeCircle(){
        gl2.glClear(GL2.GL_COLOR_BUFFER_BIT);
        gl2.glLoadIdentity();
        // рисуем круг
        gl2.glBegin(GL2.GL_TRIANGLES);
        for(int deg = 0; deg < 360; deg++){
            setHSVColor(deg, 0, 100);
            gl2.glVertex2f(centerX, centerY);
            setHSVColor(deg, 100, 100);
            gl2.glVertex2d(centerX + cos(toRadians(deg-0.5))*radius, centerY + sin(toRadians(deg-0.5))*radius);
            gl2.glVertex2d(centerX + cos(toRadians(deg+0.5))*radius, centerY + sin(toRadians(deg+0.5))*radius);
        }
        gl2.glEnd();
    }

    /**
     * проверка попадания клика в ту или иную область, соответствующая обработка
     */
    protected static void performClick(int x, int y){
        // попадание в круг
        if ((x-centerX)*(x-centerX) + (y-centerY)*(y-centerY) < (int)radius*radius){
            if (!badBlackPixel(x, y)){
                circleX = x;
                circleY = y;
            }
        } else {
            if (onBrightnessClick(x, y)) {
                rectangleY = y;
            }
        }
        updateCanvas();
    }

    /**
     * Отрисовка виджета выбора яркости, меток
     */
    protected static void updateCanvas(){
        // рисуем полоску
        FloatBuffer buffer = FloatBuffer.allocate(3);
        gl2.glReadBuffer(GL2.GL_FRONT);
        gl2.glReadPixels(circleX, circleY, 1, 1, GL2.GL_RGB, GL2.GL_FLOAT, buffer);
        gl2.glBegin(GL2.GL_QUADS);
        gl2.glColor3f(0, 0, 0);
        gl2.glVertex2f(width* brightnessWidgetPosition[0], height* brightnessWidgetPosition[2]);
        gl2.glVertex2f(width* brightnessWidgetPosition[1], height* brightnessWidgetPosition[2]);
        gl2.glColor3f(buffer.get(0), buffer.get(1), buffer.get(2));
        gl2.glVertex2f(width* brightnessWidgetPosition[1], height* brightnessWidgetPosition[3]);
        gl2.glVertex2f(width* brightnessWidgetPosition[0], height* brightnessWidgetPosition[3]);
        gl2.glEnd();
        // получаем, наконец, итоговый цвет
        gl2.glReadPixels(rectangleX, rectangleY, 1, 1, GL2.GL_RGB, GL2.GL_FLOAT, buffer);
        selectedColor = new Color(buffer.get(0), buffer.get(1), buffer.get(2));
        // рисуем метки
        makeShortTargetMark();
        makeLongTargetMark();

    }

    public static Color getSelectedColor() {
        return selectedColor;
    }

    /**
     * проверка попадания по виджету яркости
     */
    private static boolean onBrightnessClick(float x, float y){
        return  (x/width < brightnessWidgetPosition[1]) &&
                (x/width > brightnessWidgetPosition[0]) &&
                (y/height >= brightnessWidgetPosition[2]) &&
                (y/height < brightnessWidgetPosition[3]);
    }

    /**
     * отрисовка крестика на месте клика внутри круга
     */
    private static void makeShortTargetMark(){
        gl2.glBegin(GL2.GL_LINES);
        gl2.glColor3f(0, 0, 0);
        gl2.glVertex2f(circleX-5, circleY);
        gl2.glVertex2f(circleX-1, circleY);
        gl2.glVertex2f(circleX+1, circleY);
        gl2.glVertex2f(circleX+5, circleY);
        gl2.glVertex2f(circleX, circleY-5);
        gl2.glVertex2f(circleX, circleY-1);
        gl2.glVertex2f(circleX, circleY+1);
        gl2.glVertex2f(circleX, circleY+5);
        gl2.glVertex2f(circleX+1, circleY-5);
        gl2.glVertex2f(circleX+1, circleY-1);
        gl2.glVertex2f(circleX+1, circleY+1);
        gl2.glVertex2f(circleX+1, circleY+5);
        gl2.glVertex2f(circleX-3, circleY+1);
        gl2.glVertex2f(circleX+3, circleY+1);
        gl2.glVertex2f(circleX-3, circleY-1);
        gl2.glVertex2f(circleX+3, circleY-1);
        gl2.glEnd();
    }

    /**
     * отрисовка полосок вокруг выбранного цвета на шкале яркости
     */
    private static void makeLongTargetMark(){
        int delta = (int)((brightnessWidgetPosition[1]-brightnessWidgetPosition[0])*0.6*width);
        gl2.glBegin(GL2.GL_LINES);
        gl2.glColor3f(1, 1, 1);
        gl2.glVertex2f(rectangleX-delta, rectangleY-1);
        gl2.glVertex2f(rectangleX+delta, rectangleY-1);
        gl2.glVertex2f(rectangleX-delta, rectangleY+1);
        gl2.glVertex2f(rectangleX+delta, rectangleY+1);
        gl2.glEnd();
    }

    /**
     * 2 треугольника, залитых выбранным цветом
     */
    private static void showResult(){
        gl2.glBegin(GL2.GL_TRIANGLES);
        gl2.glColor3ub((byte)selectedColor.getRed(), (byte)selectedColor.getGreen(),
                (byte)selectedColor.getBlue());
        gl2.glVertex2f(0, height*0.8f);
        gl2.glVertex2f(0.2f*width, height);
        gl2.glVertex2f(0, height);
        gl2.glVertex2f(0, height*0.2f);
        gl2.glVertex2f(0.2f*width, 0);
        gl2.glVertex2f(0, 0);
        gl2.glEnd();
    }

    /**
     * Никто не идеален( Окружность тоже. Такая вот подстраховка
     */
    private static boolean badBlackPixel(int x, int y){
        try{
            FloatBuffer buffer = FloatBuffer.allocate(3);
            gl2.glReadBuffer(GL2.GL_FRONT);
            gl2.glReadPixels(x, y, 1, 1, GL2.GL_RGB, GL2.GL_FLOAT, buffer);
            return ((buffer.get(0) == 0) && (buffer.get(1) == 0) &&(buffer.get(3) == 0));
        } catch (Exception e){
            return true;
        }
    }
}
