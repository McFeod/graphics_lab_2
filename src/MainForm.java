import javax.media.opengl.GLAutoDrawable;
import javax.media.opengl.GLEventListener;
import javax.media.opengl.awt.GLJPanel;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class MainForm extends JFrame{
    private GLJPanel gljPanel;
    private JPanel root;
    private JLabel redLabel;
    private JLabel greenLabel;
    private JLabel blueLabel;
    private JTextField result;
    private int lastClickX=-10, lastClickY=-10;

    public MainForm() throws HeadlessException {

        super("Селектор цвета");
        setContentPane(root);
        pack();
        gljPanel.addGLEventListener(new GLEventListener() {
            @Override
            public void reshape(GLAutoDrawable canvas, int x, int y, int width, int height) {
                Renderer.setup(canvas.getGL().getGL2(), width, height);
            }
            @Override
            public void init(GLAutoDrawable canvas) {

            }
            @Override
            public void dispose(GLAutoDrawable canvas) {
            }
            @Override
            public void display(GLAutoDrawable canvas) {
                Renderer.render(lastClickX, lastClickY);
            }
        });

        gljPanel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent mouseEvent) {
                super.mouseClicked(mouseEvent);
                lastClickX = mouseEvent.getX();
                lastClickY = mouseEvent.getY();
                gljPanel.display();
                Color color = Renderer.getSelectedColor();
                redLabel.setText(String.valueOf(color.getRed()));
                greenLabel.setText(String.valueOf(color.getGreen()));
                blueLabel.setText(String.valueOf(color.getBlue()));
                result.setText(Integer.toHexString(color.getRGB()).substring(2).toUpperCase());
            }
        });

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setVisible(true);
    }

    public static void main(String[] args){
        MainForm form = new MainForm();
    }
}
